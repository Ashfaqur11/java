package leetcode.problems.twosum;

import java.util.Arrays;

public class Runner {

    public static void main (String args[]){
        System.out.println("Hello world");
        Solution solution = new Solution();

        int[] task = new int[]{2,7,11,15};
        int target = 9;
        int[] sol = solution.twoSum(task,target);
        System.out.println("Solution " + Arrays.toString(sol));
        System.out.println("------------------------");
        task = new int[]{3,2,4};
        target = 6;
        sol = solution.twoSum(task,target);
        System.out.println("Solution " + Arrays.toString(sol));
        System.out.println("------------------------");
        task = new int[]{3,3};
        target = 6;
        sol = solution.twoSum(task,target);
        System.out.println("Solution " + Arrays.toString(sol));
        System.out.println("------------------------");
        task = new int[]{3,2,3};
        target = 6;
        sol = solution.twoSum(task,target);
        System.out.println("Solution " + Arrays.toString(sol));
        System.out.println("------------------------");
    }
}
