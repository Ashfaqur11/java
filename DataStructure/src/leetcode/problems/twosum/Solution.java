package leetcode.problems.twosum;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Solution {

    public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        Map<Integer,Integer> map = new HashMap<Integer,Integer>();
        for (int i = 0; i < nums.length; i++) {
            int require =  target - nums[i];
            if(map.containsKey(require)){
                result[0] = map.get(require);
                result[1] = i;
                break;
            }
            map.put(nums[i],i);
        }
        return result;
    }
    public int[] twoSum1(int[] nums, int target) {
        for (int i=0; i < nums.length -1; i++){
            for (int j=i+1; j<nums.length; j++){
                if (nums[i]+nums[j] == target){
                    return new int[]{i,j};
                }
            }
        }
        return null;
    }

    public int[] twoSum2(int[] nums, int target) {
        for (int i=0; i < nums.length -1; i++){
            int findNumber = target - nums[i];
            for (int j=i+1; j<nums.length; j++){
                if (nums[j] == findNumber){
                    return new int[]{i,j};
                }
            }
        }
        return null;
    }

    public int[] twoSum3(int[] givenArray, int target) {
        int[] nums = Arrays.copyOf(givenArray, givenArray.length);
        System.out.println("Given Array: " + Arrays.toString(nums));
        Arrays.sort(nums);
        System.out.println("Sorted Array: " + Arrays.toString(nums));
        int[] foundNumbers = null;
        for (int i=0; i < nums.length -1; i++){
            int findNumber = target - nums[i];
            System.out.println("Find Number: " + findNumber);
            int[] searchArray = Arrays.copyOfRange(nums, i+1, nums.length);
            System.out.println("Search Array: " + Arrays.toString(searchArray));
            int j = Arrays.binarySearch(searchArray,findNumber);
            System.out.println(j);
            if(j>=0){
                foundNumbers = new int[]{nums[i],nums[j+i+1]};
                break;
            }
        }
        if (foundNumbers != null){
            int[] resultIndices = new int[2];
            for(int i=0; i< givenArray.length; i++){
                if (givenArray[i] == foundNumbers[0]){
                    resultIndices[0] = i;
                    System.out.println("First Number: " + i);
                    break;
                }
            }
            for(int i=0; i< givenArray.length; i++){
                if (i != resultIndices[0] && givenArray[i] == foundNumbers[1]){
                    resultIndices[1] = i;
                    System.out.println("Second Number: " + i);
                    break;
                }
            }
            return resultIndices;
        }
        return null;
    }
}
