package tutorials.sort;

public interface ISort {

    public int[] sort(int[] array);
}
