package tutorials.sort;

import java.util.Arrays;

public class MergeSort implements ISort{

    @Override
    public int[] sort(int[] array) {
        return mergeSort(array);
    }


    public int[] mergeSort(int[] array){
        if (array.length == 1){
            return array;
        }
        int mid = array.length/2;
        int[] left = Arrays.copyOfRange(array, 0, mid);
        int[] right = Arrays.copyOfRange(array, mid, array.length);
        mergeSort(left);
        mergeSort(right);
        return merge(array, left, right);
    }

    public int[] merge(int[] array, int[] left, int[] right){
        int k = 0;
        int i = 0;
        int j = 0;
        while (i < left.length && j < right.length){
            if (left[i]<=right[j]){
                array[k] = left[i];
                i++;
            } else {
                array[k] = right[j];
                j++;
            }
            k++;
        }
        while (i < left.length){
            array[k] = left[i];
            i++;
            k++;
        }
        while (j < right.length){
            array[k] = right[j];
            j++;
            k++;
        }
        return array;
    }

    @Override
    public String toString(){
        return "Merge Sort";
    }
}
