package tutorials.sort;

public class InsertionSort implements ISort{
    @Override
    public int[] sort(int[] array) {

        for (int i = 0; i < array.length; i++) {
            int j = i;
            while (j > 0 && array[j-1] > array[j]){
                swap(array,j-1,j);
                j--;
            }
        }
        return array;
    }

    public void swap(int[] array, int i, int j){
        int temp = array[j];
        array[j] = array[i];
        array[i] = temp;
    }

    @Override
    public String toString(){
        return "Insertion Sort";
    }
}
