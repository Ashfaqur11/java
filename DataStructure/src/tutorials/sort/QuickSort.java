package tutorials.sort;

import java.util.Arrays;

public class QuickSort implements ISort{
    @Override
    public int[] sort(int[] array) {
        quickSort(array, 0, array.length-1);
        return array;
    }

    public void quickSort(int[] array, int start, int end){
        if (start < end){
            int partitionIndex = partition(array, start, end);
            quickSort(array, start, partitionIndex-1);
            quickSort(array, partitionIndex+1 , end);
        }
    }

    public int partition(int[] array, int start, int end){
        int m = start + (end - start) / 2;
        swap(array, m, end);
        int pivot = array[end];
        int partitionIndex = start;
        for (int i = start; i < end; i++) {
            if (array[i] <= pivot){
                swap(array, partitionIndex, i);
                partitionIndex++;
            }
        }
        swap(array, partitionIndex, end);
        return partitionIndex;
    }

    public void swap(int[] array, int i, int j){
        if (i==j){
            return;
        }
        int temp = array[j];
        array[j] = array[i];
        array[i] = temp;
    }

    @Override
    public String toString(){
        return "Quick Sort";
    }
}
