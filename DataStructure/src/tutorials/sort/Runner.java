package tutorials.sort;

import java.util.Arrays;

public class Runner {
    public static void main(String[] args) {

        int[] input = new int[] {5,2,3,1};
        int[] expected = new int[] {1,2,3,5};
        ISort sorter = new QuickSort();
        printResults(sorter, input,expected);

//        input = new int[] {2,4,1,6,8,5,3,7};
//        expected = new int[] {1,2,3,4,5,6,7,8};
//        printResults(sorter, input,expected);
    }

    public static void printResults(ISort sorter, int[] input, int[] expected){
        int[] output = sorter.sort(Arrays.copyOf(input, input.length));
        System.out.println("Input    : " + Arrays.toString(input));
        System.out.println("Expected : " + Arrays.toString(expected));
        System.out.println("Output   : " + Arrays.toString(output));
        boolean match = Arrays.equals(expected,output);
        if (match){
            System.out.println(sorter + " Success: The result is a match");
        } else {
            System.out.println(sorter + " Failure: The result do not match");
        }

        System.out.println("-------------------------------------");
    }
}
