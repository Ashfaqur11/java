package tutorials.sort;

public class BubbleSort implements ISort{

    @Override
    public int[] sort(int[] array){
        for (int j=0; j < array.length; j++){
            int counter = 0;
            for (int i=0; i < array.length-1-counter; i++){
                if (array[i] > array[i+1] ){
                    swap(array,i,i+1);
                }
            }
            counter++;
        }
        return array;
    }

    public void swap(int[] array, int i, int j){
        int temp = array[j];
        array[j] = array[i];
        array[i] = temp;
    }

    @Override
    public String toString(){
        return "Bubble Sort";
    }
}
