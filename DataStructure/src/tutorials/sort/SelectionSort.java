package tutorials.sort;

public class SelectionSort implements ISort{
    @Override
    public int[] sort(int[] array) {

        for (int j = 0; j < array.length; j++){
            int min = array[j];
            int minIndex = -1;
            for (int i = j+1; i < array.length; i++) {
                if (array[i] < min){
                    minIndex = i;
                    min = array[i];
                }
            }
            if (minIndex != -1){
                swap(array,j,minIndex);
            }
        }
        return array;
    }

    public void swap(int[] array, int i, int j){
        int temp = array[j];
        array[j] = array[i];
        array[i] = temp;
    }

    @Override
    public String toString(){
        return "Selection Sort";
    }
}
