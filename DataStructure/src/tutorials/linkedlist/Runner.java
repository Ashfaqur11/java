package tutorials.linkedlist;

public class Runner {

    public static void main(String[] args){
        LinkedList list = new LinkedList();
        int[] array = new int[]{6,3,4,2,1};
        for (int i = 0; i < array.length; i++) {
            int value = array[i];
            list.addLast(value);
        }

        list.printList();
        System.out.println("Size of the list is " + list.getSize());
    }
}
