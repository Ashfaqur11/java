package tutorials.linkedlist;

import java.util.ArrayList;
import java.util.List;

public class LinkedList {

    private Node headNode;

    public void addLast(int value){
        Node newNode = new Node(value);

        if (this.headNode == null){
            this.headNode =  newNode;
        } else {
            Node currentNode = headNode;
            while (currentNode.getNextNode()!= null){
                currentNode = currentNode.getNextNode();
            }
            currentNode.setNextNode(newNode);
        }
    }

    public void add(int position, int value){
        Node node = new Node(value);
        if (this.headNode == null){
            if (position !=0){
                throw new IndexOutOfBoundsException("Index '" + position + "' exceeds bounds");
            }
            this.headNode = node;
        } else {
            if (position == 0){
                node.setNextNode(this.headNode);
                this.headNode = node;
            } else {
                int counter = 1;
                Node currentNode = this.headNode;
                while (counter++ != position){
                    currentNode = currentNode.getNextNode();
                    if (currentNode == null){
                        throw new IndexOutOfBoundsException("Index '" + position + "' exceeds bounds");
                    }
                }
                node.setNextNode(currentNode);
            }
        }

    }

    public void printList(){
        List<Integer> list = new ArrayList<>();
        Node node = headNode;
        while (node != null){
            list.add(node.getNodeValue());
            node = node.getNextNode();
        }
        System.out.println(list);
    }

    public int getSize(){
        int counter = 0;
        if (headNode == null){
            return counter;
        }
        Node currentNode = headNode;
        counter++;
        while (currentNode.getNextNode() != null){
            counter++;
            currentNode = currentNode.getNextNode();
        }
        return  counter;
    }
}
