package tutorials.linkedlist;

public class Node {

    private Node nextNode;
    private int nodeValue;

    public Node(int value){
        this.nodeValue = value;
    }

    public int getNodeValue(){
        return this.nodeValue;
    }

    public Node getNextNode(){
        return this.nextNode;
    }

    public void setNextNode(Node next){
        this.nextNode = next;
    }
}
