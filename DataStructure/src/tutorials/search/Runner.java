package tutorials.search;

public class Runner {

    public static void main(String[] args) {
        int[] array = new int[]{-1,0,3,5,9,12};
        int target = 9;
        int expected = 4;
        BinarySearch search = new BinarySearch();
        int output;
//        output = search.find(array,0, array.length, target);
//        System.out.println("Expected: " + expected + " Output: " + output);

        array = new int[]{-1,0,3,5,9,12};
        target = 13;
        output = search.find(array,0, array.length-1, target);
        System.out.println("Expected: " + expected + " Output: " + output);
    }
}
