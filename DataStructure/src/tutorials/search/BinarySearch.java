package tutorials.search;

public class BinarySearch {

    public int find(int[] array, int start, int end, int target){
        if (end<start){
            return  -1;
        }
        int mid = (start + end) / 2;
        if (array[mid] == target){
            return mid;
        }
        if (target < array[mid]){
            return find(array, start, mid-1, target);
        } else {
            return find(array, mid + 1, end, target);
        }
    }
}
